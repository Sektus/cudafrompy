#ifndef CUDAFROMPY_VECTOR_H
#define CUDAFROMPY_VECTOR_H

#ifdef __cplusplus
extern "C" {
#endif 

void gpuVectorSumWrapper(int * __restrict__ a, int *__restrict__ b, int * __restrict__ c, int n, int threads, cudaStream_t stream);

#ifdef __cplusplus
}
#endif

#endif