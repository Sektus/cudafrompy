#include <cuda_runtime.h>
#include <vector/vector.h>

/* Vector sum on device 
* Arguments are pointers to device memory space
* __restrict__ means that memory doesnt overlap
*/
__global__ void gpuVectorSum(int * __restrict__ a, int * __restrict__ b, int * __restrict__ c, int n)
{
    int idx = threadIdx.x + blockIdx.x * blockDim.x;

    /* check array bounds */
    if (idx < n){
        c[idx] = a[idx] + b[idx];
    }
}

void gpuVectorSumWrapper(int * __restrict__ a, int *__restrict__ b, int * __restrict__ c, int n, int threads, cudaStream_t stream)
{
    int blocks = (n + threads - 1) / threads;

    gpuVectorSum<<<blocks, threads, 0, stream>>>(a, b, c, n);
}