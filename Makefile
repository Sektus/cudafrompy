TARGET=./lib/libcudafrompy.so

OBJDIR = ./obj
SRCDIR = ./vector
LIBDIR = ./lib

NVCC = nvcc
NVCCFLAGS = -arch=compute_50 -code=sm_50 -m64 --use_fast_math -O3 -Xcompiler -fPIC
LDFLAGS = -L/usr/local/cuda/lib64 -lcurand -lm
INCLUDES = -I. -I/usr/local/cuda/include/ -I./vector

COMPILE.cu = $(NVCC) -x cu $(NVCCFLAGS) $(INCLUDES)  -dc

SRCS = $(shell find ./vector -type f -name "*.cu")
HEADS = $(shell find ./vector -type f -name "*.h")
OBJS = $(SRCS:$(SRCDIR)/%.cu=$(OBJDIR)/%.o)

.PHONY: clean all python

all: $(TARGET) python 

$(TARGET): $(OBJS) $(HEADS) | $(LIBDIR)
	$(NVCC) $(NVCCFLAGS) -o $@ --shared $(OBJS) $(LDFLAGS)
	cp $@ ~/.local/lib

$(OBJDIR)/%.o: $(SRCDIR)/%.cu | $(OBJDIR)
	$(COMPILE.cu) $(OUTPUT_OPTION) $<

$(LIBDIR): ;	@mkdir -p $@
$(OBJDIR): ;	@mkdir -p $@

python: | $(TARGET)
	python3 setup.py install --user

clean:
	$(RM) $(TARGET) $(OBJS)
