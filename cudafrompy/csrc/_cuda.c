#define PY_SSIZE_T_CLEAN
//c headers
#include <time.h>

//python headers
#include <Python.h>
#include <structmember.h>

//numpy headers
#include <numpy/arrayobject.h>

//cuda runtime header
#include <cuda_runtime.h>

//module headers
#include <vector/vector.h>


static PyObject *
_cuda_getDeviceCount(PyObject *self, PyObject *args)
{
    int devs = 0;
    if (cudaGetDeviceCount(&devs) != cudaSuccess) {
        PyErr_SetString(PyExc_RuntimeError, "No device found");
        return NULL;
    };
    return Py_BuildValue("i", devs);
} 

static PyObject *
_cuda_setDevice(PyObject *self, PyObject *args)
{
    int idx;
    int devs;

    if (!PyArg_ParseTuple(args, "i", &idx)) {
        return NULL;
    }

    if (cudaGetDeviceCount(&devs) != cudaSuccess) {
        PyErr_SetString(PyExc_RuntimeError, "No CUDA capable device found");
        return NULL;
    }

    if (idx >= devs) {
        PyErr_Format(PyExc_RuntimeError, "Index = %d is out of range(%d)", idx, devs);
        return NULL;
    } else {
        if (cudaSetDevice(idx) == cudaErrorDeviceAlreadyInUse) {
            PyErr_Format(PyExc_RuntimeError, "Device %d is busy.", idx);
            return NULL;
        }
    } 

    return Py_BuildValue("");
}

static PyObject *
_cuda_memInfo(PyObject *self, PyObject *args) 
{
    size_t free, total;

    if (cudaMemGetInfo(&free, &total) != cudaSuccess) {
        PyErr_SetString(PyExc_RuntimeError, "Cuda is not available");
        return NULL;
    }

    return Py_BuildValue("{s:I,s:I}", "free", (unsigned int) free, "total", (unsigned int) total);
}

static PyObject * 
_cuda_deviceReset(PyObject *self, PyObject *args) {
    cudaDeviceReset();
    return Py_BuildValue("");
}

static PyObject *
_cuda_add(PyObject *self, PyObject *args){
    PyObject *a = NULL, *b = NULL;
    int threads = 128;
    if (!PyArg_ParseTuple(args, "OO|i", &a, &b, &threads)) {
        return NULL;
    }

    //check if object is instance of ndarray
    if (!PyArray_Check(a) || !PyArray_Check(b)){
        PyErr_SetString(PyExc_TypeError, "ndarrays required");
        return NULL;
    }

    //check dimensions of ndarrays
    int a_shape = PyArray_NDIM(a);
    int b_shape = PyArray_NDIM(b);

    if ( a_shape != 1 || b_shape != 1){
        PyErr_Format(PyExc_AttributeError, "Vectors required. Given dims %d and %d", a_shape, b_shape);
        return NULL;
    }

    long dim = PyArray_DIM(a, 0);
    long bdim = PyArray_DIM(b, 0);

    if (dim != bdim){
        PyErr_Format(PyExc_AttributeError, "operands could not be broadcast together with shapes (%d,) (%d,) ", dim, bdim);
        return NULL;
    }

    size_t size = dim * sizeof(int);

    //get pointers to carray
    int *h_a = PyArray_DATA((PyArrayObject *)a);
    int *h_b = PyArray_DATA((PyArrayObject *)b);
    //allocate carray for output 
    int *h_c = (int *)malloc(size);

    int *d_a, *d_b, *d_c;

    cudaMalloc((void**)&(d_a), size);
    cudaMalloc((void**)&(d_b), size);
    cudaMalloc((void**)&(d_c), size);

    cudaMemcpy(d_a, h_a, size, cudaMemcpyHostToDevice);
    cudaMemcpy(d_b, h_b, size, cudaMemcpyHostToDevice);

    cudaEvent_t start, stop;
    cudaEventCreate(&start);
    cudaEventCreate(&stop);

    cudaStream_t stream;
    cudaStreamCreate(&stream);

    cudaEventRecord(start, stream);
    gpuVectorSumWrapper(d_a, d_b, d_c, dim, threads, stream);
    cudaEventRecord(stop, stream);

    cudaEventSynchronize(stop);
    float time = 0.0f;
    cudaEventElapsedTime(&time, start, stop);

    //milliseconds to seconds
    time /= 1000.0f;

    cudaMemcpy(h_c, d_c, size, cudaMemcpyDeviceToHost);

    PyObject *c = (PyObject *) PyArray_SimpleNewFromData(1, &dim, NPY_INT, (void *)h_c);
    cudaStreamDestroy(stream);
    return Py_BuildValue("Of", c, time);
}

static PyMethodDef _cuda_methods[] = {
    { "get_device_count", _cuda_getDeviceCount, METH_NOARGS, "Get number of availiable devices" },
    { "set_device", _cuda_setDevice, METH_VARARGS, "Set current CUDA context"},
    { "mem_info", _cuda_memInfo, METH_NOARGS, "Get current device memory usage" },
    { "device_reset", _cuda_deviceReset, METH_NOARGS, "Reset current device state and destroy all allocations performed" },
    { "add", _cuda_add, METH_VARARGS, "Add two vectors"},
    {NULL, NULL, 0, NULL}
};

static PyModuleDef _cudamodule = {
    PyModuleDef_HEAD_INIT,
    .m_name = "cudafrompy._cuda",
    .m_doc = "cuda extension functions",
    .m_size = -1,
    .m_methods = _cuda_methods,
};

PyMODINIT_FUNC
PyInit__cuda(void)
{
    import_array();
    
    PyObject *m = PyModule_Create(&_cudamodule);

    if (m == NULL) return NULL;
    return m;
}