#define PY_SSIZE_T_CLEAN
//python headers
#include <Python.h>
#include <structmember.h>

//person class 
#include <cudafrompy/csrc/class.h>

static PyObject *CudaFromPyError;

static PyObject * 
_c_helloWorld(PyObject *self, PyObject *args) {
    PySys_WriteStdout("Hello world!");
    return Py_BuildValue("");
}

static PyObject *
_c_raiseError(PyObject *self, PyObject *args){
    PyErr_SetString(CudaFromPyError, "Example exception");
    return NULL;
}

static PyMethodDef _c_methods[] = {
    { "hello_world", _c_helloWorld, METH_NOARGS, "Hello world function" },
    { "raise_error", _c_raiseError, METH_NOARGS, "raise cudafrompy exception"},
    {NULL, NULL, 0, NULL}
};

static PyModuleDef _cmodule = {
    PyModuleDef_HEAD_INIT,
    .m_name = "cudafrompy._c",
    .m_doc = "c extension functions",
    .m_size = -1,
    .m_methods = _c_methods,
};

PyMODINIT_FUNC
PyInit__c(void)
{
    if (PyType_Ready(&PersonType) < 0) return NULL;
    PyObject *m = PyModule_Create(&_cmodule);

    if (m == NULL) return NULL; 

    Py_INCREF(&PersonType);
    if (PyModule_AddObject(m, "Person", (PyObject *) &PersonType) < 0){
        Py_DECREF(&PersonType);
        Py_DECREF(&m);
        return NULL;
    }    

    CudaFromPyError = PyErr_NewException("cudafrompy._c.error", NULL, NULL);
    Py_XINCREF(CudaFromPyError);
    if (PyModule_AddObject(m, "cudafrompy._c.error", CudaFromPyError) < 0){
        Py_XDECREF(CudaFromPyError);
        Py_CLEAR(CudaFromPyError);
        Py_DECREF(m);
        return NULL;
    }
    return m;
}