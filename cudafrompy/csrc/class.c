#include <cudafrompy/csrc/class.h>

static PyObject *
Person_new(PyTypeObject *type, PyObject *args, PyObject *kwds)
{
    PersonObject *self;
    self = (PersonObject *) type->tp_alloc(type, 0);
    if (self != NULL){
        self->name = PyBytes_FromString("");
        if (self->name == NULL){
            Py_DECREF(self);
            return NULL;
        }
    }
    return (PyObject *) self;
}

static int
Person_init(PersonObject *self, PyObject *args, PyObject *kwds)
{
    static char *kwlist[] = {"name", NULL};
    PyObject *name = NULL, *tmp;

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "|O", kwlist, &name)) return -1;
    if (name){
        tmp = self->name;
        Py_INCREF(name);
        self->name = name;
        Py_XDECREF(tmp);
    }
    return 0;
}

static void Person_dealloc(PersonObject *self)
{
    Py_TYPE(self)->tp_free((PyObject *) self);
}

static PyObject *
Person_greet(PersonObject *self, PyObject *args, PyObject *kwds)
{
    char *name = PyUnicode_AsUTF8(self->name);
    if (name == NULL){
        return NULL;
    } 

    //print to python stdout
    PySys_FormatStdout("Hello %s!\n", name);
    
    return Py_BuildValue("");
}

static PyMethodDef Person_methods[] = {
    {"greet", (PyCFunction)Person_greet, METH_NOARGS, "Greet person"},
    {NULL}
};

static PyMemberDef Person_members[] = {
    {"name", T_OBJECT_EX, offsetof(PersonObject, name), 0,
     "person name"},
    {NULL}  /* Sentinel */
};

PyTypeObject PersonType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = "Person",
    .tp_doc = "Person class",
    .tp_basicsize = sizeof(PersonObject),
    .tp_itemsize = 0,
    .tp_flags = Py_TPFLAGS_DEFAULT | Py_TPFLAGS_BASETYPE,
    .tp_new = Person_new,
    .tp_init = (initproc) Person_init,
    .tp_dealloc = (destructor) Person_dealloc,
    .tp_methods = Person_methods,
    .tp_members = Person_members
};