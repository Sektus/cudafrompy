#ifndef CUDAFROMPY_CUDA_CLASS_H
#define CUDAFROMPY_CUDA_CLASS_H

#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <structmember.h>

typedef struct {
    PyObject_HEAD
    PyObject *name;
} PersonObject;

extern PyTypeObject PersonType;

#endif