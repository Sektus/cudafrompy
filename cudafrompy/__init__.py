import cudafrompy._cuda
import cudafrompy._c

def mem_info():
    res = cudafrompy._cuda.mem_info()
    free = res.get("free", 0)
    total = res.get("total", 0)

    gb = 1024 * 1024 * 1024
    print("Free %.2fGB of %.2fGB" % (free / gb, total / gb))
    
    