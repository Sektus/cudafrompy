from distutils.core import setup, Extension 
from pathlib import Path 
import numpy as np 

home = str(Path.home()) 
print(home)

_cuda = Extension('cudafrompy._cuda',
                  sources = ['cudafrompy/csrc/_cuda.c'],
                  include_dirs=[".","./vector", np.get_include(),'/usr/local/cuda/include', "/opt/cuda/include"],
                  library_dirs=['%s/.local/lib' % home, '/usr/local/cuda/lib64', "/opt/cuda/lib64"],
                  libraries=['cudart', 'cudafrompy'])

_c = Extension('cudafrompy._c',
                sources = ['cudafrompy/csrc/_c.c', 'cudafrompy/csrc/class.c'],
                 include_dirs=["."])

setup(name = 'cudafrompy',
      version = '0.1',
      author = 'Timur Porokhnia',
      author_email='tporohnia@gmail.com',
      description='Using CUDA in Python',
      packages=['cudafrompy'],
      #ext_package='brown',
      ext_modules=[_c, _cuda])
