# Requirements
- Python3
- Pandas
- Matplotlib
- CUDA
- Make

# Description
An example demonstrating how to wrap CUDA library with CPython and build it to use in python.

# Files 

* cuda_cpython.pdf - presentation in Polish language
* vector - CUDA library to be wrapped
* jupyter - expamples
* cudafrompy - CPython wrapper of CUDA library
* setup.py - module installation


# Usage 

```bash
make #build CUDA library
```
```bash
make python #install CPython extension user-only
```

# Note

LD_LIBRARY_PATH should be configured to include ~/.local/lib  
Jupyter notebook should be configured to include user private bin
```bash
jupyter notebook --generate-config #generate config file
jupyter --config-dir #shows config dir

#open file jupyter_notebook_config.py with vim or another editor you like
vim ~/.jupyter/jupyter_notebook_config.py

```
Add this lines:

```python
import os                                                                       
from pathlib import Path
                                                                                                                                         
c = get_config()                                                                
os.environ['LD_LIBRARY_PATH'] = f'{Path.home()}/.local/lib/'                    
c.Spawner.env.update('LD_LIBRARY_PATH')
```
